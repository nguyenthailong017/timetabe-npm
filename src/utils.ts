import { ONE_LINE_HOUR_HEIGHT, ONE_LINE_MINUTE_HEIGHT, TOTAL_MINUTES } from "./constant";


export const converHourOfTimeTable = (data: { hour: string, endHour: string }) => {
    const hour = data?.hour > data?.endHour ? convert24Hour(data?.hour) : data?.hour;
    const _endHour = data?.hour > data?.endHour ? convert24Hour(data?.endHour) : data?.endHour;
    const startTime = hour?.split(':');
    const endTime = _endHour?.split(':');
    const startHour = Number(startTime?.[0]);
    const startMinute = Number(startTime?.[1]);
    const endHour = Number(endTime?.[0]);
    const endMinute = Number(endTime?.[1]);
    return { startHour, endHour, startMinute, endMinute };
  };
  
  export const calcMarginTopOfTimeTable = (startMinute: number) =>
    ((ONE_LINE_MINUTE_HEIGHT * 5) / TOTAL_MINUTES) * startMinute +
    (startMinute !== 0 ? ONE_LINE_HOUR_HEIGHT : 0);
  
  export const calcHeightOfTimeTable = (data: { hour: string, endHour: string }) => {
    const { startHour, endHour, startMinute, endMinute } = converHourOfTimeTable(data);
    const result =
      ONE_LINE_MINUTE_HEIGHT * ((endHour - startHour) * 6) +
      (endHour - startHour) * 6 +
      (startMinute === 0 ? ONE_LINE_HOUR_HEIGHT : 0) +
      ((ONE_LINE_MINUTE_HEIGHT * 5) / TOTAL_MINUTES) * (endMinute - startMinute);
    const height = Math.ceil(result);
    return height;
  };
  
  export const convert24Hour = (hour: string) => {
    const pathHour = hour?.split(':');
    const result = pathHour?.[0] === '00' ? '24:' + pathHour?.[1] : hour;
    return result;
  };
  
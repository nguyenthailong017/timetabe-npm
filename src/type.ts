export interface IWeek {
    color: string;
    label: string;
    value: number;
    colorItem: string;
}

export  interface IValue {
    subjectId: string;
    weekday: string;
    hour: Date;
    endHour: Date;
    subject: string;
}
   
export  interface ITime {
    hour: string;
    endHour: string;
}
  
import React from 'react';
import { HOURS, MINI_MINUTE_HEIGHT, WEEK } from './constant';
import { IValue, IWeek } from './type';
import { calcHeightOfTimeTable, calcMarginTopOfTimeTable, converHourOfTimeTable } from './utils';

function App({ data, week = WEEK,  hour = HOURS }: { data?: IValue[]; hour?: string[], week?: IWeek[] }) {
  const renderTimeLineView = (item?: IWeek) => {
    return (
      <div style={{ display: 'flex', flexDirection: 'column', position: 'absolute', top: 50, width: '90%'}}>
        {hour.map((_hour) => (
          <div key={_hour} style={{ display: 'flex', flexDirection: 'column' }}>
            <span
              style={{ fontSize: 18, color: '#888888', fontWeight: 500, opacity: item ? 0 : 100 }}
            >
              {_hour}:00
              {[1, 2, 3, 4, 5]?.map((value) => (
                <p key={value} style={{ fontSize: 12, fontWeight: 200, margin: '4px 0' }}>
                  {`${_hour}:${value}0`}
                </p>
              ))}
            </span>
            {data?.map((item: any, index: number) => {
              const { startHour, startMinute, endMinute } = converHourOfTimeTable(item as any);
              const height = calcHeightOfTimeTable(item as any);
              const marginTop = calcMarginTopOfTimeTable(startMinute);
              const minutes = startMinute ? endMinute + (60 - startMinute) : endMinute;
              return (
                <div
                  key={index}
                  style={{
                    height,
                    marginTop,
                    padding: 8,
                    borderRadius: 6,
                    cursor: 'pointer',
                    zIndex: 40,
                    display: Number(_hour) === startHour && Number(item?.weekday) === item?.value ? 'flex' : 'none',
                    maxHeight: '100%',
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    textAlign: 'left',
                    flexDirection: 'column',
                    overflow: 'hidden',
                    border: 1,
                    backgroundColor: item?.colorItem
                  }}
                >
                  {height <= MINI_MINUTE_HEIGHT ? (
                    <div
                      style={{ display: 'flex', justifyContent: 'start', alignItems: 'center', height: '100%' }}>
                      <span
                        style={{ color: '#888888', fontSize: 16, fontWeight: 500 }}>{`${minutes}'`}</span>
                      &#160;
                      <span
                        style={{ fontSize: 18, fontWeight: 500 }}>
                        {item?.subject}
                      </span>
                    </div>
                  ) : (
                    <>
                      <span
                        style={{ fontSize: 15, color: '#888888', fontWeight: 500 }}>{`${item?.hour
                          }${' - '}${item?.endHour}`}</span>
                      <div
                        style={{ display: 'flex', justifyContent: 'start', alignItems: 'center', height: '100%' }}>
                        <span
                          style={{ fontSize: 17, fontWeight: 500 }}>
                          {item?.subject}
                        </span>
                      </div>
                    </>
                  )}
                </div>
              );
            })}
          </div>
        ))}
      </div>
    );
  };

  return (
        <div
      style={{ backgroundColor: 'white', width: '100%', height: '100vh', position: 'relative', overflowY: 'scroll', overflowX: 'hidden'}}>
        <div style={{position: 'fixed', top: 0, left: 0, right: 0, height: 40, width: '100%', backgroundColor: 'white', zIndex: 49}} />
      <div style={{ display: 'flex', flexGrow: 7, marginLeft: 70}}>
        {week?.map((_week: IWeek, index: number) => (
          <div key={index}
            style={{ position: 'relative', display: 'flex', justifyContent: 'center', alignItems: 'center', textAlign: 'center', width: '100%'}}
          >
            <div
              style={{ zIndex: 50, marginLeft: 10, width: '100%' }}
            >
              <div
                style={{ position: 'fixed', color: '#000000', backgroundColor: _week?.color, borderRadius: 6, zIndex: 50, padding: '10px 0', width: '12%'}}
              >{_week.label}
              </div>
            </div>
            {renderTimeLineView(_week)}
          </div>
        ))}
      </div>
      {renderTimeLineView()}
    </div>
  );
}

export default App;

export const TOTAL_LINE_OF_HOUR = 6;
export const ONE_LINE_MINUTE_HEIGHT = 16;
export const ONE_LINE_HOUR_HEIGHT = 18;
export const TOTAL_MINUTES = 59;
export const MINI_MINUTE_HEIGHT = 86;

export const HOURS = [
  '00',
  '01',
  '02',
  '03',
  '04',
  '05',
  '06',
  '07',
  '08',
  '09',
  '10',
  '11',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
  '20',
  '21',
  '22',
  '23',
];


export const WEEK = [
  {
    colorItem: '#DEF4FA',
    color: '#59C7E8',
    label: 'monday',
    value: 0,
  },
  {
    colorItem: '#D6ECE6',
    color: '#9DCABE',
    label: 'tuesday',
    value: 1,
  },
  {
    colorItem: '#E3EFC8',
    color: '#B9D675',
    label: 'wednesday',
    value: 2,
  },
  {
    colorItem: '#FFF2D3',
    color: '#FFDE90',
    label: 'thursday',
    value: 3,
  },
  {
    colorItem: '#FEE6D9',
    color: '#FBAC7F',
    label: 'friday',
    value: 4,
  },
  {
    colorItem: '#F0CDC9',
    color: '#D98277',
    label: 'saturday',
    value: 5,
  },
  {
    colorItem: '#E3E7EC',
    color: '#B8B8B8',
    label: 'sunday',
    value: 6,
  },
];
